package samplehack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;



import org.json.JSONArray;
public class StudyTest {
	/*
	 * static List<Integer> IdList = new ArrayList<>(); static List<Integer>
	 * amountList = new ArrayList<>();
	 */
	public static List<List<Integer>> arr ;
	
	public static String formatAmount(String amount) {
		String result =amount.substring(1).replace(",","");
    	String[] getdt = result.split("\\.");
    	String mainWord = getdt[0];
    	return mainWord;
	}
	
	
	public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
		
		 List<List<Integer>> result =totalTransaction("1", "debit");
		 System.out.println(result);
		
	}
	
	
	public static List<List<Integer>> totalTransaction(String location,String transactionType) {
		
		  Integer newAmount = null;
		  Integer currentPage=1;
		  HashMap<Integer, Integer> my_dict = new HashMap<Integer, Integer>();	
	      String baseURL = "https://jsonmock.hackerrank.com/api/transactions/search?txnType=debit&page="+currentPage.toString();
	      String json = getData(baseURL);
	      JSONObject jsonObject = new JSONObject(json);
	      Integer totalpage =jsonObject.optInt("total_pages");
	         for (int i =1;i<=totalpage;i++) {
	        	  baseURL = "https://jsonmock.hackerrank.com/api/transactions/search?txnType=debit&page="+String.valueOf(i);
	   	          json = getData(baseURL);
	   	          jsonObject = new JSONObject(json);
	        	  JSONArray dataArray = jsonObject.optJSONArray("data");
		    	  for(int j=0; j<dataArray.length();j++) { 
		    	  JSONObject dataF= dataArray.getJSONObject(j);
		  	      JSONObject img= dataF.getJSONObject("location");
		          String id =img.optString("id");
		          if(id.equals(location)) {
		        	  Integer userId=dataF.optInt("userId");
		        	  String amount =dataF.optString("amount");
		        	  if(!my_dict.containsKey(userId)) {
		        	  my_dict.put(userId,Integer.parseInt(formatAmount(amount)));
		        	  }
		        	  else
		        	  {
		        		  amount =dataF.optString("amount");
		        		  newAmount=my_dict.get(userId)+ Integer.parseInt(formatAmount(amount));
		        		  my_dict.put(userId,newAmount);
		        	  }
	         }
	    	 
	          }
	          
	    }
	         List<List<Integer>> arr = new ArrayList<List<Integer>>();
	     	
         ArrayList<Integer> keyList = new ArrayList<Integer>(my_dict.keySet());
	         keyList.sort(null);
	    //    System.out.println(keyList);
	         for(int j=0;j<keyList.size();j++)
	         {
	        	 List<Integer> myList = new ArrayList<>();
	        	 myList.add(keyList.get(j));
	        	 myList.add(my_dict.get(keyList.get(j)));
	        	 arr.add(myList);
	         }
            
	     	return arr;
			
			}
	
 public static String getData(String url) {
        StringBuilder sb = new StringBuilder();
        URLConnection urlConnection = null;
        InputStreamReader in = null;
        try {
            URL url1 = new URL(url);
            urlConnection = url1.openConnection();
            if (urlConnection != null)
                urlConnection.setReadTimeout(60 * 1000);
            if (urlConnection != null && urlConnection.getInputStream() != null) {
                in = new InputStreamReader(urlConnection.getInputStream(), Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();

    }
}


 